<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Create new user</title>
	</head>
	<body>
	
		<form:form modelAttribute="newUser">
			<form:label path="login">Login </form:label>
			<form:input path="login"/>
			<br>
			<form:label path="password">Password</form:label>
			<form:password path="password"/>		
			<br>
			<form:label path="email">Email</form:label>
			<form:input path="email"/>
			<br>
			<input type="radio" name="authority" value="ROLE_USER" checked>USER<br>
			<input type="radio" name="authority" value="ROLE_ADMIN">ADMIN<br>
			<br>
			<input type="submit" value="Submit"/>
		</form:form>
		<br>
		
	</body>
</html>