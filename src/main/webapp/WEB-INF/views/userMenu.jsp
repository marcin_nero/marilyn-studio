<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Menu</title>
		<link href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"
        rel="stylesheet">
	</head>
	<body>
	
		<c:if test="${isSuccess }">
			<label class="alert alert-success">${success}</label>
		</c:if>
		
		${message}.
		<br>
		<br>
		<table>
			<thead>
				<tr>
					<th>Menu</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>
						Schedule menu
					</th>
				</tr>
				<tr>
					<td>
						<a href="<spring:url value="#"/>"> 
							Update existing schedule
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<a href="<spring:url value="#"/>">
							Create new month schedule
						</a>
					</td>
				</tr>
				<tr>
					<th>
						User menu
					</th>
				</tr>
				<tr>
					<td>
						<a href="<spring:url value="/updateUser"/>">
							Update user information
						</a>
					</td>
				</tr>
				<sec:authorize access="hasRole('ADMIN')">
					<tr>
						<td>
							<a href="<spring:url value="/newUser"/>">
								Add new user
							</a>
						</td>
					</tr>
					<tr>
						<td>
							<a href="<spring:url value="/getUserList"/>">
								List of users
							</a>
						</td>
					</tr>
				</sec:authorize>
				<tr>
					<td>
						<a href="<spring:url value="/logout"/>" class="btn btn-primary">Logout</a>
					</td>
				</tr>
			</tbody>
		</table>
		
        <script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</body>
</html>