<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Users list</title>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th>User ID</th>
					<th>Login</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.user_id}</td>
						<td>${user.login}</td>
						<td>${user.email}</td>
						<td><a href="<spring:url value="/deleteUser${user.user_id}"/>">Delete user</a></td>
					</tr>
				</c:forEach>			
			</tbody>
		</table>
		<a href="<spring:url value="/user"/>" type="">Menu</a>
	</body>
</html>