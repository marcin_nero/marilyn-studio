<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html 	>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<style>
		
			table { width: 50%; box-sizing: border-box; box-shadow: inset 0px 0px 0px 1px rgba(0,0,0,0.1); font-family: 'Ubuntu'; /*border-collapse: collapse;*/ border-spacing: 0; background-color: #333333; }

			th, td { border: 1px solid rgba(255,255,255,0.1); box-sizing: border-box; text-align: center; }

			th { text-transform: uppercase; font-size:10px; font-weight:700; padding: 10px 0; color: rgba(255,255,255,0.5); background-color: #292929; letter-spacing: 1px; }

			td { width: 14.285%; transition: all 0.3s; font-size: 14px; color: rgba(255,255,255,0.6); font-weight: 400; font-size: 14px; padding: 1.5% 1.5% 5%; vertical-align: initial; padding: 1.5% 0 ; }
			
			.day:hover { background-color: rgba(0,0,0,0.1); cursor:pointer; }
			
			.today { color: #FFF; background-color: rgba(0,0,0,.25) !important; }

			span.number { margin-left: 10% }
		</style>
	</head>
	<body>
	
	<br>
		${month}
	<br>	
		<table>
			<tr>
				<th>Monday</th>
				<th>Tuesday</th>
				<th>Wednesday</th>
				<th>Thursday</th>
				<th>Friday</th>
				<th>Saturday</th>
				<th>Sunday</th>
			</tr>
			
			<c:set var="day" value="1" />			
			
			<c:forEach var="weeks" begin="1" end="${(daysInMonth / 7) + 1 }" >
				<tr>
					<c:forEach var="days" begin="1" end="7">
						<c:choose>
							<c:when test="${weeks == 1 && dayOfWeek != 1 }">
								<c:choose>
									<c:when test="${days < dayOfWeek }">
										<td class="day"></td>
									</c:when>
									<c:otherwise>
										<td class="day">
											<span class="number">${day} </span>
											<c:if test="${day < daysInMonth }">
												<c:set var="day" value="${day + 1}"/>
											</c:if>
											
										</td>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<td class="day">
									<c:if test="${day <= daysInMonth }">
										<span class="number">${day } </span>
										<c:set var="day" value="${day + 1}"/>
									</c:if>
								</td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</tr>
			</c:forEach>
			
		</table>
	</body>
</html>