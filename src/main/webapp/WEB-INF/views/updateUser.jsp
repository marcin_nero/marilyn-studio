<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Update User Information</title>
		<link href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"
        rel="stylesheet">
	</head>
	<body>
	
		<c:if test="${isError}">
			<label class="alert alert-danger">${error}</label>
		</c:if>
	
		<form:form modelAttribute="user">
			<form:label path="email">Login: </form:label>
			<form:input path="email"/>
			<br>
			<form:label path="password">Password:</form:label>
			<input type="password" name="password">
			<br>
			<c:set var="passwordTest" />
			<strong>Retype password:</strong> 
			<input type="password" name="passwordTest">
			<br>
			<form:input path="user_id" type="hidden" value="${user.user_id }"/>
			<form:input path="login" type="hidden" value="${user.login }"/>
			<input type="submit" value="Submit" class="btn btn-default"/>
			<a href="<spring:url value="/user"/>">Back</a>
			
		</form:form>
		
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</body>
</html>