<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Marilyn Hair & Beauty Studio</title>
</head>
<body>
	<h1>Spring Security JDBC Authentication Example</h1>
	<h2>${message}</h2>
	
	
	<c:choose>
		<c:when test="${schedule==false }">
			<button >8:15 false</button>
		</c:when>
		<c:otherwise>
			<button>8:15 true</button>
		</c:otherwise>
	</c:choose>
	
	
	<a href="<spring:url value="/user" />" >Zaloguje się</a>
	<a href="<c:url value="/logout" /> " >Wyloguj się</a>
	
	<br>
	email - ${user}
	
	<br>
	<br>
	<br>
	
	
	
</body>
</html>