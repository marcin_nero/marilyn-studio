package com.marilynstudio.service;

import java.util.List;

import com.marilynstudio.domain.User;

public interface UserService {
	
	public void createUser(User user, String authority);
	public void deleteUser(String user_id);
	public User readUser(String login);
	public void updateUserEmail(User user);
	public void updateUserPassword(User user);
	public List<User> getUserList();
}
