package com.marilynstudio.service;

import java.time.LocalDate;
import java.util.List;

import com.marilynstudio.domain.Schedule;

public interface ScheduleService {

	public void updateSchedule(Schedule schedule);
	public Schedule getSchedule(String user_login, LocalDate date);
	public void deleteSchedule(String user_login, LocalDate date);
	public void createSchedule(String user_login,LocalDate date);
}
