package com.marilynstudio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marilynstudio.dao.UserDAO;
import com.marilynstudio.domain.User;
import com.marilynstudio.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDao;
	
	@Override
	public void createUser(User user, String authority) {
				
		userDao.createUser(user, authority);
	}

	@Override
	public void deleteUser(String user_id) {
		
		userDao.deletUser(user_id);
	}

	@Override
	public User readUser(String login) {
		
		return userDao.readUser(login);
	}

	@Override
	public void updateUserEmail(User user) {
		
		userDao.updateUserEmail(user);
	}
	
	@Override
	public void updateUserPassword(User user) {
		userDao.updateUserPassword(user);
	}

	@Override
	public List<User> getUserList() {
		
		return userDao.getUserList();
	}

}
