package com.marilynstudio.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marilynstudio.dao.ScheduleDAO;
import com.marilynstudio.domain.Schedule;
import com.marilynstudio.service.ScheduleService;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	private ScheduleDAO scheduleDao;
	
	@Override
	public void updateSchedule(Schedule schedule) {
		
		scheduleDao.updateSchedule(schedule);
		
	}

	@Override
	public Schedule getSchedule(String user_login, LocalDate date) {
		
		return scheduleDao.getSchedule(user_login, date);
	}

	@Override
	public void deleteSchedule(String user_login, LocalDate date) {
		
		scheduleDao.deleteSchedule(user_login, date);
		
	}
	
	@Override
	public void createSchedule(String user_login, LocalDate date) {
		scheduleDao.createSchedule(user_login, date);
	}

}
