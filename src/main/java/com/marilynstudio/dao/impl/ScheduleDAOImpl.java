package com.marilynstudio.dao.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.marilynstudio.dao.ScheduleDAO;
import com.marilynstudio.domain.Schedule;

@Repository
public class ScheduleDAOImpl implements ScheduleDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void updateSchedule(Schedule schedule) {
		// To do
		
		
			
	}

	@Override
	public Schedule getSchedule(String user_login, LocalDate date) {
		String sql = "SELECT schedule_id, schedule.user_id, schedule_date, start_work_hour, "
				+ "end_work_hour FROM schedule INNER JOIN user ON "
				+ "schedule.user_id = user.user_id WHERE user.login = \'" + user_login + "\' AND "
				+ " schedule.schedule_date = \'" + date + "\'";
		return jdbcTemplate.query(sql, new ResultSetExtractor<Schedule>() {
			@Override
			public Schedule extractData(ResultSet rs) throws SQLException, DataAccessException {
				if(rs.next()) {
					Schedule schedule = new Schedule();
					schedule.setSchedule_id(rs.getInt("schedule_id"));
					schedule.setUser_id(rs.getInt("user_id"));
					schedule.setSchedule_date(rs.getDate("schedule_date").toLocalDate());
					schedule.setStart_work_hour(rs.getInt("start_work_hour"));
					schedule.setEnd_work_hour(rs.getInt("end_work_hour"));
					schedule.setIsAvailable(getAvailableHoursFlag(schedule));
					
					System.out.println(schedule.getSchedule_id());
					System.out.println(schedule.getUser_id());
					System.out.println(schedule.getSchedule_date());
					System.out.println(schedule.getStart_work_hour());
					System.out.println(schedule.getEnd_work_hour());
					System.out.println(schedule.getIsAvailable().toString());
					
					return schedule;
				}
				return null;
			}
		});
	}

	@Override
	public void deleteSchedule(String user_login, LocalDate date) {
		
		// to do
		
	}
	
	@Override
	public void createSchedule(String user_login, LocalDate date) {

		// to do
		String sql = "";
		
	}

	@Override
	public void createNewMonthSchedule(String userLogin) {
		
	// to do
		
	}
	
	private List<Boolean> getAvailableHoursFlag(Schedule schedule){
		String sql = "SELECT * FROM availability WHERE schedule_id = " + schedule.getSchedule_id();
		List<Boolean> availableHoursFlag = jdbcTemplate.query(sql, new RowMapper<Boolean>() {
			@Override
			public Boolean mapRow(ResultSet rs, int rowNum) throws SQLException {
				Boolean isAvailable = new Boolean(rs.getBoolean("isAvailable"));		
				return isAvailable;
			}
		});
		return availableHoursFlag;
	}

}
