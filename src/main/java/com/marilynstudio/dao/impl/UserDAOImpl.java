package com.marilynstudio.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.marilynstudio.dao.UserDAO;
import com.marilynstudio.domain.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void createUser(User user, String authority) {

		String sql = "INSERT INTO user (login, password, email, enabled) VALUES (?,?,?,?)";
		String sql_auth = "INSERT INTO authorities (login, authority) VALUES (?,?)";
		
		String pas = new BCryptPasswordEncoder().encode(user.getPassword());
		
		jdbcTemplate.update(sql, user.getLogin(), pas, user.getEmail(), true);
		
		jdbcTemplate.update(sql_auth, user.getLogin(), authority);
		
	}

	@Override
	public void deletUser(String user_id) {
		
		String sql = "DELETE FROM user WHERE user_id = ?";
		String sql_auth = "DELETE FROM authorities WHERE login = (SELECT login FROM user WHERE user_id = ?)";
		
		
		
		jdbcTemplate.update(sql_auth, user_id);
		
		jdbcTemplate.update(sql, user_id);
		
	}

	@Override
	public User readUser(String login) {
		
		String sql = "SELECT * FROM user WHERE login=\'" + login + "\'";

		return jdbcTemplate.query(sql, new ResultSetExtractor<User>() {

			@Override
			public User extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {	
					User user = new User();
					user.setUser_id(rs.getInt("user_id"));
					user.setLogin(rs.getString("login"));
					user.setPassword(rs.getString("password"));
					user.setEmail(rs.getString("email"));

					return user;
				}
				return null;
			}

		});
	}

	@Override
	public void updateUserEmail(User user) {

		String sql = "UPDATE user SET email=? WHERE user_id=?";
		
		jdbcTemplate.update(sql, user.getEmail(), user.getUser_id());
	}
	
	@Override
	public void updateUserPassword(User user) {
		String sql = "UPDATE user SET password=? WHERE user_id=?";
		
		String pas = new BCryptPasswordEncoder().encode(user.getPassword());
		
		jdbcTemplate.update(sql, pas, user.getUser_id());
	}

	@Override
	public List<User> getUserList() {
		
		String sql = "SELECT * FROM user";
		
		List<User> userList = jdbcTemplate.query(sql, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				
				user.setUser_id(rs.getInt("user_id"));
				user.setLogin(rs.getString("login"));
				user.setEmail(rs.getString("email"));
				
				return user;
			}
			
		});
		
		return userList;
	}

}
