package com.marilynstudio.controller;

import java.security.Principal;
import java.time.LocalDate;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.marilynstudio.domain.Schedule;
import com.marilynstudio.domain.User;
import com.marilynstudio.service.ScheduleService;
import com.marilynstudio.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ScheduleService scheduleService;
	
	@GetMapping("/test")
	public String test(Model model) {
		
		model.addAttribute("message", "to jest test");
		
		User user = userService.readUser("marcin_nero");
		
		model.addAttribute("user", user.getEmail() );
		
		LocalDate date = LocalDate.of(2019, 1, 15);
		
		Schedule sched = scheduleService.getSchedule("marcin_nero", date);
		
	
		
		
		return "index";
	}
	
	@GetMapping("/user")
	public String user(Model model, Principal principal) {
		
		model.addAttribute("message", "You are logged in as " + principal.getName());
		model.addAttribute("login", principal.getName());
		
		return "userMenu";
	}
	
	@GetMapping("/newUser")
	public String newUserForm(Model model) {
		
		User newUser = new User();
		String authority = "";
		model.addAttribute("newUser", newUser);
		model.addAttribute("authority", authority);
		
		return "newUser";
	}
	
	@PostMapping("/newUser")
	public String processNewUser(@Valid @ModelAttribute("newUser") User newUser, String authority, BindingResult result) {
		
		if(result.hasErrors()) {
			return "newUser";
		}
		
		userService.createUser(newUser, authority);
		
		return "redirect:/user";
	}
	
	@GetMapping("/getUserList")
	public String getUserList(Model model) {
		
		model.addAttribute("userList", userService.getUserList());
		
		return "userList";
	}
	
	@GetMapping("/deleteUser{user_id}")
	public String deleteUser(@PathVariable String user_id, Model model) {
		
		userService.deleteUser(user_id);
		
		return "redirect:/getUserList";
	}
	
	@GetMapping("/updateUser")
	public String updateUser(Model model, Principal principal) {
		
		User user = userService.readUser(principal.getName());
		model.addAttribute("user", user);
		
		return "updateUser";
	}
	
	@PostMapping("/updateUser")
	public String processUpdateUser(@ModelAttribute User user,String password, String passwordTest, Model model) {
		
		if(!password.equals(passwordTest)) {
			model.addAttribute("isError", true);
			model.addAttribute("error", "Type equal password in both lines");
			return "updateUser";
		}
		
		if(password == "") {
			userService.updateUserEmail(user);
		}else {
			userService.updateUserPassword(user);
			userService.updateUserEmail(user);
		}
		
		
		return "redirect:/user";
	}
}
