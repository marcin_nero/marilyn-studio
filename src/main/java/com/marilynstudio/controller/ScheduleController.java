package com.marilynstudio.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.marilynstudio.domain.Schedule;
import com.marilynstudio.service.ScheduleService;
import com.marilynstudio.service.UserService;

@Controller
public class ScheduleController {

	@Autowired
	private ScheduleService scheduleService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/actual_calendar")
	public String getActualMonthCalendar(Model model) {
		
		LocalDate today = LocalDate.of(2019, 02, 12);
		LocalDate firstOfMonth = LocalDate.of(today.getYear(), today.getMonth(), 1);
		int daysInMonth = today.lengthOfMonth();
		int dayOfWeek = firstOfMonth.getDayOfWeek().getValue();
		
		model.addAttribute("daysInMonth", daysInMonth);
		model.addAttribute("dayOfWeek", dayOfWeek);
		model.addAttribute("month", today.getMonth());
		model.addAttribute("thisDay", today.getDayOfMonth());
		model.addAttribute("today", today.getDayOfMonth());
		
		return "calendar";
	}
	
	@GetMapping("/next_month_calendar")
	public String getNextMontCalendar(Model model) {
		
		LocalDate nextMonth = LocalDate.now().plusMonths(1);
		LocalDate firstOfMonth = LocalDate.of(nextMonth.getYear(), nextMonth.getMonth(), 1);
		int daysInMonth = nextMonth.lengthOfMonth();
		int dayOfWeek = firstOfMonth.getDayOfWeek().getValue();
		
		model.addAttribute("daysInMonth", daysInMonth);
		model.addAttribute("dayOfWeek", dayOfWeek);
		model.addAttribute("month", nextMonth.getMonth());
		model.addAttribute("thisDay", nextMonth.getDayOfMonth());
		
		return "calendar";
	}
	
	@GetMapping("/calendar/{user_login}/{date}")
	public String getUserCalendar(Model model, @PathVariable String user_login, @PathVariable String date ) {
		
		LocalDate d = LocalDate.parse(date);
		System.out.println(scheduleService.getSchedule(user_login, d).getIsAvailable().size());
		
		model.addAttribute("schedule", scheduleService.getSchedule(user_login, d));
		
		System.out.println(scheduleService.getSchedule(user_login, d).getIsAvailable().size());
		
		return "schedule";
	}
}
