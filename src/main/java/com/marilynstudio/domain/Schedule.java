package com.marilynstudio.domain;

import java.time.LocalDate;
import java.util.List;

public class Schedule {
	private int schedule_id;
	private int user_id;
	private LocalDate schedule_date;
	private int start_work_hour;
	private int end_work_hour;
	private List<Boolean> isAvailable;
	
	public int getSchedule_id() {
		return schedule_id;
	}
	public void setSchedule_id(int schedule_id) {
		this.schedule_id = schedule_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public LocalDate getSchedule_date() {
		return schedule_date;
	}
	public void setSchedule_date(LocalDate schedule_date) {
		this.schedule_date = schedule_date;
	}
	public int getStart_work_hour() {
		return start_work_hour;
	}
	public void setStart_work_hour(int start_work_hour) {
		this.start_work_hour = start_work_hour;
	}
	public int getEnd_work_hour() {
		return end_work_hour;
	}
	public void setEnd_work_hour(int end_work_hour) {
		this.end_work_hour = end_work_hour;
	}
	public List<Boolean> getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(List<Boolean> isAvailable) {
		this.isAvailable = isAvailable;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + schedule_id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Schedule other = (Schedule) obj;
		if (schedule_id != other.schedule_id)
			return false;
		return true;
	}

	
}
