package com.marilynstudio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class MarilynStudioApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(MarilynStudioApplication.class, args);
	}
}
